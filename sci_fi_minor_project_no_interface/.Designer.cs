﻿
namespace sci_fi_minor_project_no_interface
{
	public partial class Btp 
	{
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.labelHP = new System.Windows.Forms.Label();
			this.labelEXP = new System.Windows.Forms.Label();
			this.labelLVL = new System.Windows.Forms.Label();
			this.labelEnergy = new System.Windows.Forms.Label();
			this.lblhp = new System.Windows.Forms.Label();
			this.lblexp = new System.Windows.Forms.Label();
			this.lbllvl = new System.Windows.Forms.Label();
			this.lblenergy = new System.Windows.Forms.Label();
			this.lblSelectaction = new System.Windows.Forms.Label();
			this.cboWeapons = new System.Windows.Forms.ComboBox();
			this.cboBoosts = new System.Windows.Forms.ComboBox();
			this.btnUseWeapon = new System.Windows.Forms.Button();
			this.btnUseBoost = new System.Windows.Forms.Button();
			this.btnWest = new System.Windows.Forms.Button();
			this.btnSouth = new System.Windows.Forms.Button();
			this.btnEast = new System.Windows.Forms.Button();
			this.btnNorth = new System.Windows.Forms.Button();
			this.rtbLocation = new System.Windows.Forms.RichTextBox();
			this.rtbMessages = new System.Windows.Forms.RichTextBox();
			this.dgvQuests = new System.Windows.Forms.DataGridView();
			this.dgvInventory = new System.Windows.Forms.DataGridView();
			((System.ComponentModel.ISupportInitialize)(this.dgvQuests)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvInventory)).BeginInit();
			this.SuspendLayout();
			// 
			// labelHP
			// 
			this.labelHP.AutoSize = true;
			this.labelHP.Location = new System.Drawing.Point(18, 20);
			this.labelHP.Name = "labelHP";
			this.labelHP.Size = new System.Drawing.Size(21, 13);
			this.labelHP.TabIndex = 0;
			this.labelHP.Text = "HP";
			// 
			// labelEXP
			// 
			this.labelEXP.AutoSize = true;
			this.labelEXP.Location = new System.Drawing.Point(18, 46);
			this.labelEXP.Name = "labelEXP";
			this.labelEXP.Size = new System.Drawing.Size(26, 13);
			this.labelEXP.TabIndex = 1;
			this.labelEXP.Text = "EXP";
			// 
			// labelLVL
			// 
			this.labelLVL.AutoSize = true;
			this.labelLVL.Location = new System.Drawing.Point(18, 74);
			this.labelLVL.Name = "labelLVL";
			this.labelLVL.Size = new System.Drawing.Size(23, 13);
			this.labelLVL.TabIndex = 2;
			this.labelLVL.Text = "LVL";
			// 
			// labelEnergy
			// 
			this.labelEnergy.AutoSize = true;
			this.labelEnergy.Location = new System.Drawing.Point(18, 100);
			this.labelEnergy.Name = "labelEnergy";
			this.labelEnergy.Size = new System.Drawing.Size(42, 13);
			this.labelEnergy.TabIndex = 3;
			this.labelEnergy.Text = "Energy";
			// 
			// lblhp
			// 
			this.lblhp.AutoSize = true;
			this.lblhp.BackColor = System.Drawing.SystemColors.Control;
			this.lblhp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblhp.Location = new System.Drawing.Point(110, 19);
			this.lblhp.Name = "lblhp";
			this.lblhp.Size = new System.Drawing.Size(2, 15);
			this.lblhp.TabIndex = 4;
			// 
			// lblexp
			// 
			this.lblexp.AutoSize = true;
			this.lblexp.BackColor = System.Drawing.SystemColors.Control;
			this.lblexp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblexp.Location = new System.Drawing.Point(110, 45);
			this.lblexp.Name = "lblexp";
			this.lblexp.Size = new System.Drawing.Size(2, 15);
			this.lblexp.TabIndex = 5;
			// 
			// lbllvl
			// 
			this.lbllvl.AutoSize = true;
			this.lbllvl.BackColor = System.Drawing.SystemColors.Control;
			this.lbllvl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbllvl.Location = new System.Drawing.Point(110, 73);
			this.lbllvl.Name = "lbllvl";
			this.lbllvl.Size = new System.Drawing.Size(2, 15);
			this.lbllvl.TabIndex = 6;
			// 
			// lblenergy
			// 
			this.lblenergy.AutoSize = true;
			this.lblenergy.BackColor = System.Drawing.SystemColors.Control;
			this.lblenergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblenergy.Location = new System.Drawing.Point(110, 99);
			this.lblenergy.Name = "lblenergy";
			this.lblenergy.Size = new System.Drawing.Size(2, 15);
			this.lblenergy.TabIndex = 7;
			// 
			// lblSelectaction
			// 
			this.lblSelectaction.AutoSize = true;
			this.lblSelectaction.Location = new System.Drawing.Point(617, 531);
			this.lblSelectaction.Name = "lblSelectaction";
			this.lblSelectaction.Size = new System.Drawing.Size(71, 13);
			this.lblSelectaction.TabIndex = 8;
			this.lblSelectaction.Text = "Select action";
			// 
			// cboWeapons
			// 
			this.cboWeapons.FormattingEnabled = true;
			this.cboWeapons.Location = new System.Drawing.Point(369, 559);
			this.cboWeapons.Name = "cboWeapons";
			this.cboWeapons.Size = new System.Drawing.Size(121, 21);
			this.cboWeapons.TabIndex = 9;
			// 
			// cboBoosts
			// 
			this.cboBoosts.FormattingEnabled = true;
			this.cboBoosts.Location = new System.Drawing.Point(369, 593);
			this.cboBoosts.Name = "cboBoosts";
			this.cboBoosts.Size = new System.Drawing.Size(121, 21);
			this.cboBoosts.TabIndex = 10;
			// 
			// btnUseWeapon
			// 
			this.btnUseWeapon.Location = new System.Drawing.Point(620, 559);
			this.btnUseWeapon.Name = "btnUseWeapon";
			this.btnUseWeapon.Size = new System.Drawing.Size(75, 23);
			this.btnUseWeapon.TabIndex = 11;
			this.btnUseWeapon.Text = "Use";
			this.btnUseWeapon.UseVisualStyleBackColor = true;
			this.btnUseWeapon.Click += new System.EventHandler(this.btnUseWeapon_Click);
			// 
			// btnUseBoost
			// 
			this.btnUseBoost.Location = new System.Drawing.Point(620, 593);
			this.btnUseBoost.Name = "btnUseBoost";
			this.btnUseBoost.Size = new System.Drawing.Size(75, 23);
			this.btnUseBoost.TabIndex = 12;
			this.btnUseBoost.Text = "Use";
			this.btnUseBoost.UseVisualStyleBackColor = true;
			this.btnUseBoost.Click += new System.EventHandler(this.btnUseBoost_Click);
			// 
			// btnWest
			// 
			this.btnWest.Location = new System.Drawing.Point(412, 457);
			this.btnWest.Name = "btnWest";
			this.btnWest.Size = new System.Drawing.Size(75, 23);
			this.btnWest.TabIndex = 13;
			this.btnWest.Text = "West";
			this.btnWest.UseVisualStyleBackColor = true;
			this.btnWest.Click += new System.EventHandler(this.btnWest_Click);
			// 
			// btnSouth
			// 
			this.btnSouth.Location = new System.Drawing.Point(493, 487);
			this.btnSouth.Name = "btnSouth";
			this.btnSouth.Size = new System.Drawing.Size(75, 23);
			this.btnSouth.TabIndex = 14;
			this.btnSouth.Text = "South";
			this.btnSouth.UseVisualStyleBackColor = true;
			this.btnSouth.Click += new System.EventHandler(this.btnSouth_Click);
			// 
			// btnEast
			// 
			this.btnEast.Location = new System.Drawing.Point(573, 457);
			this.btnEast.Name = "btnEast";
			this.btnEast.Size = new System.Drawing.Size(75, 23);
			this.btnEast.TabIndex = 15;
			this.btnEast.Text = "East";
			this.btnEast.UseVisualStyleBackColor = true;
			this.btnEast.Click += new System.EventHandler(this.btnEast_Click);
			// 
			// btnNorth
			// 
			this.btnNorth.Location = new System.Drawing.Point(493, 433);
			this.btnNorth.Name = "btnNorth";
			this.btnNorth.Size = new System.Drawing.Size(75, 23);
			this.btnNorth.TabIndex = 16;
			this.btnNorth.Text = "North";
			this.btnNorth.UseVisualStyleBackColor = true;
			this.btnNorth.Click += new System.EventHandler(this.btnNorth_Click);
			// 
			// rtbLocation
			// 
			this.rtbLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.rtbLocation.Location = new System.Drawing.Point(347, 19);
			this.rtbLocation.Name = "rtbLocation";
			this.rtbLocation.ReadOnly = true;
			this.rtbLocation.Size = new System.Drawing.Size(360, 105);
			this.rtbLocation.TabIndex = 17;
			this.rtbLocation.Text = "";
			// 
			// rtbMessages
			// 
			this.rtbMessages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.rtbMessages.EnableAutoDragDrop = true;
			this.rtbMessages.Location = new System.Drawing.Point(347, 130);
			this.rtbMessages.Name = "rtbMessages";
			this.rtbMessages.ReadOnly = true;
			this.rtbMessages.Size = new System.Drawing.Size(360, 286);
			this.rtbMessages.TabIndex = 18;
			this.rtbMessages.Text = "";
			// 
			// dgvQuests
			// 
			this.dgvQuests.AllowUserToAddRows = false;
			this.dgvQuests.AllowUserToDeleteRows = false;
			this.dgvQuests.AllowUserToResizeRows = false;
			this.dgvQuests.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
			this.dgvQuests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvQuests.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dgvQuests.Enabled = false;
			this.dgvQuests.Location = new System.Drawing.Point(16, 446);
			this.dgvQuests.MultiSelect = false;
			this.dgvQuests.Name = "dgvQuests";
			this.dgvQuests.ReadOnly = true;
			this.dgvQuests.RowHeadersVisible = false;
			this.dgvQuests.RowTemplate.Height = 24;
			this.dgvQuests.Size = new System.Drawing.Size(312, 189);
			this.dgvQuests.TabIndex = 20;
			// 
			// dgvInventory
			// 
			this.dgvInventory.AllowUserToAddRows = false;
			this.dgvInventory.AllowUserToDeleteRows = false;
			this.dgvInventory.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
			this.dgvInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvInventory.Location = new System.Drawing.Point(16, 130);
			this.dgvInventory.Name = "dgvInventory";
			this.dgvInventory.ReadOnly = true;
			this.dgvInventory.RowTemplate.Height = 24;
			this.dgvInventory.Size = new System.Drawing.Size(312, 310);
			this.dgvInventory.TabIndex = 21;
			// 
			// Btp
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(719, 651);
			this.Controls.Add(this.dgvInventory);
			this.Controls.Add(this.dgvQuests);
			this.Controls.Add(this.rtbMessages);
			this.Controls.Add(this.rtbLocation);
			this.Controls.Add(this.btnNorth);
			this.Controls.Add(this.btnEast);
			this.Controls.Add(this.btnSouth);
			this.Controls.Add(this.btnWest);
			this.Controls.Add(this.btnUseBoost);
			this.Controls.Add(this.btnUseWeapon);
			this.Controls.Add(this.cboBoosts);
			this.Controls.Add(this.cboWeapons);
			this.Controls.Add(this.lblSelectaction);
			this.Controls.Add(this.lblenergy);
			this.Controls.Add(this.lbllvl);
			this.Controls.Add(this.lblexp);
			this.Controls.Add(this.lblhp);
			this.Controls.Add(this.labelEnergy);
			this.Controls.Add(this.labelLVL);
			this.Controls.Add(this.labelEXP);
			this.Controls.Add(this.labelHP);
			this.Name = "Btp";
			this.Text = "Between the planets - alfa";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BTP_FormClosing);
			this.Load += new System.EventHandler(this.Btp_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvQuests)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvInventory)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHP;
        private System.Windows.Forms.Label labelEXP;
        private System.Windows.Forms.Label labelLVL;
        private System.Windows.Forms.Label labelEnergy;
        private System.Windows.Forms.Label lblhp;
        private System.Windows.Forms.Label lblexp;
        private System.Windows.Forms.Label lbllvl;
        private System.Windows.Forms.Label lblSelectaction;
        private System.Windows.Forms.ComboBox cboWeapons;
        private System.Windows.Forms.ComboBox cboBoosts;
        private System.Windows.Forms.Button btnUseWeapon;
        private System.Windows.Forms.Button btnUseBoost;
        private System.Windows.Forms.Button btnWest;
        private System.Windows.Forms.Button btnSouth;
        private System.Windows.Forms.Button btnEast;
        private System.Windows.Forms.Button btnNorth;
        private System.Windows.Forms.RichTextBox rtbLocation;
        private System.Windows.Forms.RichTextBox rtbMessages;
        private System.Windows.Forms.DataGridView dgvQuests;
        private System.Windows.Forms.Label lblenergy;


        private void Btp_Load(object sender, System.EventArgs e)
        {

        }

		private System.Windows.Forms.DataGridView dgvInventory;
	}

}

