﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public static class World
    {
        public static readonly List<Item> Items = new List<Item>();
        public static readonly List<Monster> Monsters = new List<Monster>();
        public static readonly List<Quest> Quests = new List<Quest>();
        public static readonly List<Location> Locations = new List<Location>();

        public const int ITEM_ID_BIG_SPANNER = 1; //ГАЕЧНЫЙ КЛЮЧ. СТАРТОВЫЙ НАБОР.
        public const int ITEM_ID_RAT_BLOOD = 2; //ОБРАЗЦЫ КРОВИ. ПРЕДМЕТ ДЛЯ КРАФТА. ПРЕДМЕТ ДЛЯ ЗАДАНИЯ
        public const int ITEM_ID_PIECE_OF_TAPE = 3; //КЛЕЙКАЯ ЛЕНТА. ПРЕДМЕТ ДЛЯ КРАФТА
        public const int ITEM_ID_HEALING_SYRINGE = 7; //ПРЕДМЕТ ДЛЯ ВОССТАНОВЛЕНИЯ ХП
        public const int ITEM_ID_CRAFT = 9;
        public const int ITEM_ID_CARD_PASS = 10;
        public const int ITEM_ID_LAZER_SWORD = 11;

        public const int MONSTER_ID_RAT = 1;
        public const int MONSTER_ID_RADIATED_CREWMATE = 2;
        public const int MONSTER_ID_RADIATED_CAPTAIN = 3;

        public const int QUEST_ID_CLEAR_LABORATORY = 1;
        

        public const int LOCATION_ID_HOME = 1;
        public const int LOCATION_ID_MAIN_HALL = 2;
        public const int LOCATION_ID_CANTEEN = 3;
        public const int LOCATION_ID_CAPTAIN_BRIDGE = 4;
        public const int LOCATION_ID_RADIO_ROOM = 5;
        public const int LOCATION_ID_STORAGE = 6;
        public const int LOCATION_ID_LABORATORY = 7;
        public const int LOCATION_ID_CABINS = 8;
        public const int LOCATION_ID_CAPTAIN_ROOM = 9;

        static World()
        {
            
            PopulateItems();
            PopulateMonsters();
            PopulateQuests();
            PopulateLocations();
            
        }

        private static void PopulateItems()
        {
            Items.Add(new Weapon(ITEM_ID_BIG_SPANNER, "Big spanner", "Big spanners", 0, 5));
            Items.Add(new Weapon(ITEM_ID_LAZER_SWORD, "Lazer sword", "Lazer swords", 4, 8));
            Items.Add(new Item(ITEM_ID_RAT_BLOOD, "Rat blood", "Rat blood"));
            //Items.Add(new Weapon(ITEM_ID_CLUB, "Club", "Clubs", 3, 10));
            Items.Add(new HealingSyringe(ITEM_ID_HEALING_SYRINGE, "Healing syringe", "Healing syringes", 5));
            Items.Add(new Item(ITEM_ID_CARD_PASS, "Card pass", "Card passes"));
        }

        private static void PopulateMonsters()
        {
            Monster rat = new Monster(MONSTER_ID_RAT, "Rat", 3, 3, 10, 3, 3);
            rat.LootTable.Add(new LootItem(ItemByID(ITEM_ID_RAT_BLOOD), 75, false));

            Monster radiated_crewmate = new Monster(MONSTER_ID_RADIATED_CREWMATE, "Radiated crewmate", 5, 5, 15, 5, 5);
            radiated_crewmate.LootTable.Add(new LootItem(ItemByID(ITEM_ID_LAZER_SWORD), 50, false));
            radiated_crewmate.LootTable.Add(new LootItem(ItemByID(ITEM_ID_HEALING_SYRINGE),75, false));

            Monster radiated_captain = new Monster(MONSTER_ID_RADIATED_CAPTAIN, "<BOSS> Radiated captain", 9, 9, 25, 20, 20);
            radiated_crewmate.LootTable.Add(new LootItem(ItemByID(ITEM_ID_HEALING_SYRINGE), 100, false));

            Monsters.Add(rat);
            Monsters.Add(radiated_crewmate);
            Monsters.Add(radiated_captain);
        }

        private static void PopulateQuests()
        {
            Quest clearLaboratory =
                new Quest(
                    QUEST_ID_CLEAR_LABORATORY,
                    "Clear the laboratory",
                    "Kill rats in the laboratory and bring back 3 experimantal bottles of blood. You will receive a healing syringe and 10 energy points.", 20, 10);

            clearLaboratory.QuestCompletionItems.Add(new QuestCompletionItem(ItemByID(ITEM_ID_RAT_BLOOD), 3));

            clearLaboratory.RewardItem = ItemByID(ITEM_ID_HEALING_SYRINGE);
            clearLaboratory.RewardItem = ItemByID(ITEM_ID_CARD_PASS);

           
            Quests.Add(clearLaboratory);
          
        }

        private static void PopulateLocations()
        {
            // Create each location
            Location home = new Location(LOCATION_ID_HOME, "Home", "This is room near reactor. Out captain said that i need to be close to it to fix in time");

            Location mainHall = new Location(LOCATION_ID_MAIN_HALL, "Main hall", "This hall seems empty.");

            Location captainBridge = new Location(LOCATION_ID_CAPTAIN_BRIDGE, "Captain bridge", "Not everybody able to get there. After all i need to make a photo as I'm a captain", ItemByID(ITEM_ID_CARD_PASS));
            captainBridge.MonsterLivingHere = MonsterByID(MONSTER_ID_RADIATED_CAPTAIN);

            Location radioRoom = new Location(LOCATION_ID_RADIO_ROOM, "Radio room", "Here I can call for help.");
            

            Location storage = new Location(LOCATION_ID_STORAGE, "Storage", "Maybie i can find something usefull here.");
            storage.QuestAvailableHere = QuestByID(QUEST_ID_CLEAR_LABORATORY);

            Location laboratory = new Location(LOCATION_ID_LABORATORY, "Laboratory", "Eww! It smells awfull.");
            laboratory.MonsterLivingHere = MonsterByID(MONSTER_ID_RAT);

            Location canteen = new Location(LOCATION_ID_CANTEEN, "Canteen", "Any delicious spacefood for dinner?");
            canteen.MonsterLivingHere = MonsterByID(MONSTER_ID_RADIATED_CREWMATE);

            Location cabins = new Location(LOCATION_ID_CABINS, "Cabins", "My room is much bigger than theese cabins. Yes, I don't care that this is just an engine room");

            Location captainRoom = new Location(LOCATION_ID_CAPTAIN_ROOM, "Captain room", "That is what I call luxury! Wait, first card.");
            

            // Link the locations together
            home.LocationToNorth = mainHall;

            mainHall.LocationToNorth = captainBridge;
            mainHall.LocationToSouth = home;
            mainHall.LocationToEast = canteen;
            mainHall.LocationToWest = storage;

            storage.LocationToEast = mainHall;
            storage.LocationToWest = laboratory;

            laboratory.LocationToEast = storage;

            captainBridge.LocationToSouth = mainHall;
            captainBridge.LocationToNorth = radioRoom;

            radioRoom.LocationToSouth = captainBridge;

            canteen.LocationToEast = cabins;
            canteen.LocationToWest = mainHall;

            cabins.LocationToWest = canteen;
            cabins.LocationToSouth = captainRoom;

            captainRoom.LocationToNorth = cabins;

            // Add the locations to the static list
            Locations.Add(home);
            Locations.Add(mainHall);
            Locations.Add(canteen);
            Locations.Add(captainBridge);
            Locations.Add(radioRoom);
            Locations.Add(storage);
            Locations.Add(laboratory);
            Locations.Add(cabins);
            Locations.Add(captainRoom);
        }

        public static Item ItemByID(int id)
        {
            foreach (Item item in Items)
            {
                if (item.ID == id)
                {
                    return item;
                }
            }

            return null;
        }

        public static Monster MonsterByID(int id)
        {
            foreach (Monster monster in Monsters)
            {
                if (monster.ID == id)
                {
                    return monster;
                }
            }

            return null;
        }

        public static Quest QuestByID(int id)
        {
            foreach (Quest quest in Quests)
            {
                if (quest.ID == id)
                {
                    return quest;
                }
            }

            return null;
        }

        public static Location LocationByID(int id)
        {
            foreach (Location location in Locations)
            {
                if (location.ID == id)
                {
                    return location;
                }
            }

            return null;
        }


    }
}
