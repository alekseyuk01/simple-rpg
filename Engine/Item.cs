﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine
{
	public class Item
	{
		public int ID { get; set; }				//>Weapon + HealingSuringe base class
		public string Name { get; set; }        //>Weapon + HealingSuringe base class
		public string NamePlural { get; set; }  //>Weapon + HealingSuringe base class
		public Item RewardItem { get; set; }


		public Item (int id, string name, string nameplural)
		{
			ID = id;
			Name = name;
			NamePlural = nameplural;
		}
	}
}
