﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine
{
	public class LootItem
	{
		public Item Details { get; set; }
		public int DropPercentage { get; set; }
		public bool IsDefaultItem { get; set; }

		public LootItem (Item details, int droppercentage, bool isdefaultitem)
		{
			Details = details;
			DropPercentage = droppercentage;
			IsDefaultItem = isdefaultitem;
			// Каждому свойству мы приписываем переменную вписанную выше 
		}
	}
}
