﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engine
{
	public class Monster : LivingCreature
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public int MaximumDamage { get; set; }
		public int RewardExperiencePoints { get; set; }
		public int RewardEnergy { get; set; }
		public List<LootItem> LootTable { get; set; }

		public Monster(int id, string name, int maximumdamage, int rewardexperiencepoints, int rewardenergy, int currenthitpoints, int maximumhitpoints) : base (currenthitpoints, maximumhitpoints)
		{
			ID = id;
			Name = name;
			MaximumDamage = maximumdamage;
			RewardExperiencePoints = rewardexperiencepoints;
			RewardEnergy = rewardenergy;

			LootTable = new List<LootItem>();
		}//constructor code
	}
}
