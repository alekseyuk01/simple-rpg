﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Engine
{
	public class InventoryItem 

	{
		public Item Details;
		public int Quantity;
		

		public InventoryItem (Item details, int quantity)
		{
			Details = details;
			Quantity = quantity;
			 
		}

	}
}
